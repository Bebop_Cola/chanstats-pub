import React, { Component } from 'react';
import { Menu } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import logo from '../yot.svg';

export default class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = { active: {}, gay: props };

    this.handleItemClick = (e, { name }) => this.setState({ activeItem: name });
  }
  render() {
    const { activeItem } = this.state.active;

    return (
      <Menu stackable inverted size="large">
        <Menu.Item>
          <img src={logo} alt={"logo"}/>
        </Menu.Item>

        <Menu.Item 
          as={ Link } 
          to="/"
          name="home"
          active={activeItem === 'home'}
          onClick={this.handleItemClick}
        >
          Home
        </Menu.Item>
        <Menu.Item 
          as={ Link } 
          to='/pol'
          name="pol"
          active={activeItem === 'pol'}
          onClick={this.handleItemClick}
        >
          /pol/
        </Menu.Item>
        <Menu.Item 
          as={ Link } 
          to="/fit"
          name="fit"
          active={activeItem === 'fit'}
          onClick={this.handleItemClick}
        >
          /fit/
        </Menu.Item>

        <Menu.Item 
          as={ Link } 
          to="/g"
          name="g"
          active={activeItem === 'g'}
          onClick={this.handleItemClick}
        >
          /g/
        </Menu.Item>

        
      </Menu>
    );
  }
}
