import React, { Component } from 'react';
import { Tab } from 'semantic-ui-react';


class Tabbed extends Component {
  constructor(props) {
    super(props);
    this.state = { tabs: props.tabs, panes: [] };
  }

  componentWillMount() {
    const topush = [];
    for (let i = 0; i < this.state.tabs.length; i += 1) {
      topush.push({ menuItem: this.state.tabs[i].name, render: this.state.tabs[i].render });
    }
    this.setState({ panes: topush });
  }

  render() {
    return (<Tab menu={{ secondary: true, pointing: true }} panes={this.state.panes} />);
  }
}
export default Tabbed;
