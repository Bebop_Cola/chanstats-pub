import React, { Component } from 'react';
import { Bar, Pie, Doughnut } from 'react-chartjs-2';
import chartOptions from '../configs/chart';
import Tabbed from '../ui/Tabbed';
import { dbRefG } from '../App';
import { Tab } from 'semantic-ui-react';

class G extends Component {
  constructor(props) {
    super(props);
    this.state = { loaded: false, data: props, gen: {}, lan: {}, cpu: {} };
  }

  componentDidMount() {
    dbRefG.once('value').then((snapshot) => {
      this.setState({ data: snapshot.val(),
        gen: {
          name: 'Generals',
          render: () => (<Tab.Pane><Bar
            data={{
              labels: [
                this.state.data.titles[0][0],
                this.state.data.titles[1][0],
                this.state.data.titles[2][0],
                this.state.data.titles[3][0],
                this.state.data.titles[4][0],
                this.state.data.titles[5][0],
                this.state.data.titles[6][0],
                this.state.data.titles[7][0],
                this.state.data.titles[8][0],
                this.state.data.titles[9][0],
              ],
              datasets: [{
                label: 'threads',
                data: [
                  this.state.data.titles[0][1],
                  this.state.data.titles[1][1],
                  this.state.data.titles[2][1],
                  this.state.data.titles[3][1],
                  this.state.data.titles[4][1],
                  this.state.data.titles[5][1],
                  this.state.data.titles[6][1],
                  this.state.data.titles[7][1],
                  this.state.data.titles[8][1],
                  this.state.data.titles[9][1],
                ],
                borderWidth: chartOptions.defaultBorderWidth,
                borderColor: chartOptions.defaultBorderColor,
                backgroundColor: chartOptions.defaultColors,
              }],
            }}
            options={{
              title: {
                text: 'Generals',
                fontFamily: chartOptions.defaultFontFamily,
                fontSize: chartOptions.defaultFontSize,
                fontColor: chartOptions.defaultFontColor,
              },
              legend: {
                labels: chartOptions.defaultLegendLabel,
                display: false,
                position: 'bottom',
              },
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero: true,
                  },
                }],
              },
            }}
          /></Tab.Pane>) },
        cpu: {
          name: 'Intel vs AMD',
          render: () => (<Tab.Pane><Pie
            data={{
              labels: [
                'AMD', 'INTEL',
              ],
              datasets: [{
                label: 'Shilling',
                data: [
                  this.state.data.languages.amd, this.state.data.languages.intel,
                ],
                borderWidth: chartOptions.defaultBorderWidth,
                borderColor: chartOptions.defaultBorderColor,
                backgroundColor: chartOptions.defaultColors,
              }],
            }}
            options={{
              title: {
                text: 'Amd vs Intel',
                fontFamily: chartOptions.defaultFontFamily,
                fontSize: chartOptions.defaultFontSize,
                fontColor: chartOptions.defaultFontColor,
              },
              legend: {
                labels: chartOptions.defaultLegendLabel,
                position: 'bottom',
              },
            }}
          /></Tab.Pane>) },
        lan: {
          name: 'Languages',
          render: () => (<Tab.Pane><Doughnut
            data={{
              labels: [
                'C',
                'Python',
                'Ruby',
                'Java',
                'JavaScript',
                'Php',
                'Lisp',
                'Perl',
                'C++',
                'Bash',
              ],
              datasets: [{
                label: 'Languages',
                data: [
                  this.state.data.languages.c,
                  this.state.data.languages.python,
                  this.state.data.languages.ruby,
                  this.state.data.languages.java,
                  this.state.data.languages.js + this.state.data.languages.javascript,
                  this.state.data.languages.php,
                  this.state.data.languages.lisp,
                  this.state.data.languages.perl,
                  this.state.data.languages['c++'],
                  this.state.data.languages.bash,
                ],
                borderWidth: chartOptions.defaultBorderWidth,
                borderColor: chartOptions.defaultBorderColor,
                backgroundColor: chartOptions.defaultColors,
              }],
            }}
            options={{
              title: {
                text: 'File Names',
                fontFamily: chartOptions.defaultFontFamily,
                fontSize: chartOptions.defaultFontSize,
                fontColor: chartOptions.defaultFontColor,
              },
              legend: {
                labels: chartOptions.defaultLegendLabel,
                position: 'bottom',
              },
            }}
          /></Tab.Pane>),
        },
        loaded: true });
    });
  }

  render() {
    let page;
    if (this.state.loaded) {
      page = (
        <Tabbed tabs={[this.state.gen, this.state.cpu, this.state.lan]} />
    );
    } else {
      page = (
        <div></div>
      );
    }
    return page;
  }
}

export default G;

