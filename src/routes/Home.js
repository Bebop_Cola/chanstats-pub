import React, { Component } from 'react';
import { Header, Divider, Message } from 'semantic-ui-react';

class Home extends Component {

  render() {
    return (
      <div>
        <Divider clearing />
        <Header textAlign="center" size="huge">
        Welcome to Chan Stats!
      </Header>
        <Header textAlign="center">(in alpha)</Header>
        <Divider />
        <Message>
          <Message.Header>Basic Gestalt</Message.Header>
          <Message.List>
            <Message.Item>
            Board information is very generic right now. This is intended for
            people who are somewhat familiar with board culture,
            i.e. You can tell if there are severe upticks in a specific filename, or patterns, it
            means bots.
            </Message.Item>
            <Message.Item>
            Information is pulled depending on traffic, but at a bare minimum of every hour.
             although for /pol/ it's every few minutes.
            </Message.Item>
            <Message.Item>
            Data over time will be made available when I have it going for a
            long enough time for it to be relevant.
            </Message.Item>
            <Message.Item>
             These starting boards were chosen because they're fairly iconic, and I only have to 
             deal with one super high traffic board.
            </Message.Item>
          </Message.List>
        </Message>
        <Message>
          <Message.Header>Things already on the list.</Message.Header>
          <Message.List>
            <Message.Item>
            Response (Sucess) rate of obvious bait threads, copy/paste threads, etc..
            </Message.Item>
            <Message.Item>
            Cross-board culture spread, i.e; image macro patterns, language patterns, etc..
            </Message.Item>
            <Message.Item>
            Timezones in relation to all of the above. (Finally catching the Aussies)
            </Message.Item>
            <Message.Item>
             Making it look nicer, Providing Raw Number Statistic Alternative view options, 
             and the like.
            </Message.Item>
            <Message.Item>
             Making it mobile friendly.             
            </Message.Item>
            <Message.Item>
             Exposing a public API
            </Message.Item>
          </Message.List>
        </Message>
        <Message negative>
    <Message.Header>Before you send me requests</Message.Header>
    <p>Please be detailed as you probably know, chan board culture and language are weird. In 
    all likelyhood I will not be as familiar with your board as you are.</p>
  </Message>
  <p className="center-text">built on  react/node</p>
      </div>
    );
  }
  }

export default Home;

