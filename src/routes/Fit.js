import React, { Component } from 'react';
import { Bar, Doughnut } from 'react-chartjs-2';
import chartOptions from '../configs/chart';
import Tabbed from '../ui/Tabbed';
import { dbRefFit } from '../App';
import { Tab } from 'semantic-ui-react';

class Fit extends Component {
  constructor(props) {
    super(props);
    this.state = { loaded: false, data: props, routine: {}, gen: {} };
  }

  componentDidMount() {
    dbRefFit.once('value').then((snapshot) => {
      this.setState({
        data: snapshot.val(),
        loaded: true,
        routine: { name: 'Routines',
          render: () => (<Tab.Pane><Doughnut
            data={{
              labels: [
                'GreySkull', 'PHUL', 'PPL', 'Reg Park', 'Strong Lifts', 'Texas Method', 'ULPP', 'Starting Strength',
              ],
              datasets: [{
                label: 'Shilling',
                data: [
                  this.state.data.routine.grayskull + this.state.data.routine.greyskull + this.state.data.routine.gslp,
                  this.state.data.routine.phul,
                  this.state.data.routine.ppl,
                  this.state.data.routine.regpark,
                  this.state.data.routine.sl + this.state.data.routine.stronglifts,
                  this.state.data.routine.texasmethod,
                  this.state.data.routine.ulpp,
                  this.state.data.routine.ss + this.state.data.routine.startingstrength,

                ],
                borderWidth: chartOptions.defaultBorderWidth,
                borderColor: chartOptions.defaultBorderColor,
                backgroundColor: chartOptions.defaultColors,
              }],
            }}
            options={{
              title: {
                text: 'Routines',
                fontFamily: chartOptions.defaultFontFamily,
                fontSize: chartOptions.defaultFontSize,
                fontColor: chartOptions.defaultFontColor,
              },
              legend: {
                labels: chartOptions.defaultLegendLabel,
                position: 'bottom',
              },
            }}
          /></Tab.Pane>) },
        gen: {
          name: 'Generals',
          render: () => (<Tab.Pane><Bar
            data={{
              labels: [
                this.state.data.fitGens[0][0],
                this.state.data.fitGens[2][0],
                this.state.data.fitGens[3][0],
                this.state.data.fitGens[4][0],
                this.state.data.fitGens[5][0],
                this.state.data.fitGens[6][0],
              ],
              datasets: [{
                label: 'Fit Generals',
                data: [
                  this.state.data.fitGens[0][1],
                  this.state.data.fitGens[2][1],
                  this.state.data.fitGens[3][1],
                  this.state.data.fitGens[4][1],
                  this.state.data.fitGens[5][1],
                  this.state.data.fitGens[6][1],
                ],
                borderWidth: chartOptions.defaultBorderWidth,
                borderColor: chartOptions.defaultBorderColor,
                backgroundColor: chartOptions.defaultColors,
              }],
            }}
            options={{
              title: {
                text: 'Generals',
                fontFamily: chartOptions.defaultFontFamily,
                fontSize: chartOptions.defaultFontSize,
                fontColor: chartOptions.defaultFontColor,
              },
              legend: {
                display: false,
              },
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero: true,
                  },
                }],
              },
            }}
          /></Tab.Pane>) },
      });
    });
  }

  render() {
    let page;
    if (this.state.loaded) {
      page = (
        <Tabbed tabs={[this.state.routine, this.state.gen]} />
    );
    } else {
      page = (
        <div />
      );
    }
    return page;
  }
}

export default Fit;
