import React, { Component } from 'react';
import { Pie, Bar, Doughnut } from 'react-chartjs-2';
import chartOptions from '../configs/chart';
import Tabbed from '../ui/Tabbed';
import { dbRefPol } from '../App';
import { Tab } from 'semantic-ui-react';

class Pol extends Component {
  constructor(props) {
    super(props);
    this.state = { loaded: false, data: {}, country: {}, gen: {}, fn: {}, dupe: {} };
  }
  componentDidMount() {
    dbRefPol.once('value').then((snapshot) => {
      this.setState({
        data: snapshot.val(),
        country: { name: 'Countries',
          render: () => (<Tab.Pane><Doughnut
            data={{
              labels: [
                this.state.data.countries[0].countryname,
                this.state.data.countries[1].countryname,
                this.state.data.countries[2].countryname,
                this.state.data.countries[3].countryname,
                this.state.data.countries[4].countryname,
                this.state.data.countries[5].countryname,
                this.state.data.countries[6].countryname,
                this.state.data.countries[7].countryname,
                this.state.data.countries[8].countryname,
                this.state.data.countries[9].countryname,
                this.state.data.countries[11].countryname,
                this.state.data.countries[12].countryname,
                this.state.data.countries[13].countryname,
                this.state.data.countries[14].countryname,
                this.state.data.countries[15].countryname,
                this.state.data.countries[16].countryname,
              ],
              datasets: [{
                label: 'Country/Flags',
                data: [
                  this.state.data.countries[0]['COUNT(*)'],
                  this.state.data.countries[1]['COUNT(*)'],
                  this.state.data.countries[2]['COUNT(*)'],
                  this.state.data.countries[3]['COUNT(*)'],
                  this.state.data.countries[4]['COUNT(*)'],
                  this.state.data.countries[5]['COUNT(*)'],
                  this.state.data.countries[6]['COUNT(*)'],
                  this.state.data.countries[7]['COUNT(*)'],
                  this.state.data.countries[8]['COUNT(*)'],
                  this.state.data.countries[9]['COUNT(*)'],
                  this.state.data.countries[10]['COUNT(*)'],
                  this.state.data.countries[11]['COUNT(*)'],
                  this.state.data.countries[12]['COUNT(*)'],
                  this.state.data.countries[13]['COUNT(*)'],
                  this.state.data.countries[14]['COUNT(*)'],
                  this.state.data.countries[15]['COUNT(*)'],
                  this.state.data.countries[16]['COUNT(*)'],
                ],
                borderWidth: chartOptions.defaultBorderWidth,
                borderColor: chartOptions.defaultBorderColor,
                backgroundColor: chartOptions.defaultColors,
              }],
            }}
            options={{
              title: {
                text: 'Country/Flag Posts',
                fontFamily: chartOptions.defaultFontFamily,
                fontSize: chartOptions.defaultFontSize,
                fontColor: chartOptions.defaultFontColor,
              },
              legend: {
                labels: {
                  fontSize: 15,
                  fontColor: 'black',
                },
                position: 'bottom',
              },
            }}
          /></Tab.Pane>) },
        fn: { name: 'FIleNames',
          render: () => (<Tab.Pane><Doughnut
            data={{
              labels: [this.state.data.fileNames[1].filename, this.state.data.fileNames[2].filename, this.state.data.fileNames[3].filename],
              datasets: [{
                label: 'File Names',
                data: [this.state.data.fileNames[1]['COUNT(*)'], this.state.data.fileNames[2]['COUNT(*)'], this.state.data.fileNames[3]['COUNT(*)']],
                borderWidth: chartOptions.defaultBorderWidth,
                borderColor: chartOptions.defaultBorderColor,
                backgroundColor: chartOptions.defaultColors,
              }],
            }}
            options={{
              title: {
                text: 'File Names',
                fontFamily: chartOptions.defaultFontFamily,
                fontSize: chartOptions.defaultFontSize,
                fontColor: chartOptions.defaultFontColor,
              },
              legend: {
                labels: chartOptions.defaultLegendLabel,
                position: 'bottom',
              },
            }}
          /></Tab.Pane>) },
        dupe: { name: 'Duplicates',
          render: () => (<Tab.Pane><Doughnut
            data={{
              labels: [
                this.state.data.dupe[0].title,
                this.state.data.dupe[2].title,
                this.state.data.dupe[3].title,
                this.state.data.dupe[4].title,
                this.state.data.dupe[5].title,
                this.state.data.dupe[6].title,
                this.state.data.dupe[7].title,
                this.state.data.dupe[8].title,
                this.state.data.dupe[9].title,
              ],
              datasets: [{
                label: 'File Names',
                data: [
                  this.state.data.dupe[0]['COUNT(*)'],
                  this.state.data.dupe[2]['COUNT(*)'],
                  this.state.data.dupe[3]['COUNT(*)'],
                  this.state.data.dupe[4]['COUNT(*)'],
                  this.state.data.dupe[5]['COUNT(*)'],
                  this.state.data.dupe[6]['COUNT(*)'],
                  this.state.data.dupe[7]['COUNT(*)'],
                  this.state.data.dupe[8]['COUNT(*)'],
                  this.state.data.dupe[9]['COUNT(*)'],
                ],
                borderWidth: chartOptions.defaultBorderWidth,
                borderColor: chartOptions.defaultBorderColor,
                backgroundColor: chartOptions.defaultColors,
              }],
            }}
            options={{
              title: {
                text: 'File Names',
                fontFamily: chartOptions.defaultFontFamily,
                fontSize: chartOptions.defaultFontSize,
                fontColor: chartOptions.defaultFontColor,
              },
              legend: {
                labels: chartOptions.defaultLegendLabel,
                position: 'bottom',
              },
            }}
          /></Tab.Pane>) },
        gen: { name: 'Generals',
          render: () => (<Tab.Pane><Pie
            data={{
              labels: [
                this.state.data.polGens[1][0],
                this.state.data.polGens[2][0],
                this.state.data.polGens[3][0],
                this.state.data.polGens[4][0],
                this.state.data.polGens[5][0],
                this.state.data.polGens[6][0],
                this.state.data.polGens[7][0],
                this.state.data.polGens[8][0],
                this.state.data.polGens[9][0],
              ],
              datasets: [{
                label: 'Threads',
                data: [
                  this.state.data.polGens[1][1],
                  this.state.data.polGens[2][1],
                  this.state.data.polGens[3][1],
                  this.state.data.polGens[4][1],
                  this.state.data.polGens[5][1],
                  this.state.data.polGens[6][1],
                  this.state.data.polGens[7][1],
                  this.state.data.polGens[8][1],
                  this.state.data.polGens[9][1],
                ],
                borderWidth: chartOptions.defaultBorderWidth,
                borderColor: chartOptions.defaultBorderColor,
                backgroundColor: chartOptions.defaultColors,
              }],
            }}
            options={{
              title: {
                text: 'Generals',
                fontFamily: chartOptions.defaultFontFamily,
                fontSize: chartOptions.defaultFontSize,
                fontColor: chartOptions.defaultFontColor,
              },
              legend: {
                labels: chartOptions.defaultLegendLabel,
                position: 'bottom',
              },
            }}
          /></Tab.Pane>) },
        loaded: true });
    });
  }

  render() {
    let page;
    if (this.state.loaded) {
      page = (
        <Tabbed tabs={[this.state.country, this.state.fn, this.state.dupe, this.state.gen]} />
    );
    } else {
      page = (
        <div>
        </div>
      );
    }
    return page;
  }
}

export default Pol;
