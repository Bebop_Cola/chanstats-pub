const chartOptions = {

  defaultColors: [
    'rgba(231, 76, 60,1.0)',
    'rgba(52, 152, 219,1.0)',
    'rgba(46, 204, 113,1.0)',
    'rgba(230, 126, 34,1.0)',
    'rgba(155, 89, 182,1.0)',
    'rgba(26, 188, 156,1.0)',
    'rgba(254, 95, 85, 1.0)',
    'rgba(241, 196, 15,1.0)',
    'rgba(52, 73, 94,1.0)',
    'rgba(149, 165, 166,1.0)',
    'rgba(192, 57, 43,1.0)',
    'rgba(41, 128, 185,1.0)',
    'rgba(39, 174, 96,1.0)',
    'rgba(211, 84, 0,1.0)',
    'rgba(142, 68, 173,1.0)',
    'rgba(22, 160, 133,1.0)',
    'rgba(243, 156, 18,1.0)',
    'rgba(189, 195, 199,1.0)',
    'rgba(44, 62, 80,1.0)',
    'rgba(127, 140, 141,1.0)'],

  defaultBorderWidth: 2,
  defaultBorderColor: '#FDFDFF',

  defaultFontFamily: 'Open Sans',
  defaultFontSize: 25,
  defaultFontColor: '#272727',

  defaultLegendLabel: {
    fontSize: 18,
    defaultFontFamily: 'Open Sans',
    fontColor: 'black',
  },

};

export default chartOptions;
