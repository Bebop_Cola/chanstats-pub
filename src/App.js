import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import { Container, Header } from 'semantic-ui-react';
import * as firebase from 'firebase';
import './App.css';
import Pol from './routes/Pol';
import G from './routes/G';
import Fit from './routes/Fit';
import Home from './routes/Home';
import NavBar from './ui/Navbar';


const config = {
  apiKey: 'XXXX',
  authDomain: 'XXXX',
  databaseURL: 'XXXX',
  projectId: 'XXXX',
  storageBucket: 'XXXX',
  messagingSenderId: 'XXXXX',
};
firebase.initializeApp(config);
export const dbRefPol = firebase.database().ref().child('pol');
export const dbRefG = firebase.database().ref().child('g');
export const dbRefFit = firebase.database().ref().child('fit');
const dbRefDate = firebase.database().ref().child('range');

class App extends Component {

  constructor() {
    super();
    this.state = { pol: {}, fit: {}, g: {}, date: { min: '0', max: '1' }, loaded: false };
  }

  componentWillMount() {
    dbRefDate.once('value').then((snapshot) => {
      const date = snapshot.val();
      const n_min = date.min.substring(5, 16);
      const n_max = date.max.substring(5, 16);
      this.setState({ date: { min: n_min, max: n_max },
      });
    });
  }
  render() {
    return (
      <Router>
        <div>
          <NavBar />
          <Container>
            <Header textAlign="center" size="small">( {this.state.date.min} - {this.state.date.max} )</Header>
            <Route exact path="/" component={Home} />
            <Route path="/fit" component={Fit} />
            <Route path="/g" component={G} />
            <Route path="/pol" component={Pol} />
          </Container>
        </div>
      </Router>
    );
  }
}

export default App;
